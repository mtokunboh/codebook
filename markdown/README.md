---
yaml: supported 
---
# Brief Markdwon (EasyMark) Tutorial

This is just the easy parts of Markdown from John Gruber. 

## Headers

Up to 6 headers, started with hashtags.

```md
# header 1

## header 2

### header 3

#### header 4

###### header 5

####### header 6 


```

## Separators

Use four dashes `----` for separators.

----


## Inline Formatting

*italic (em)*
**bold (strong)**

***bold italic (strong em)***

`monospace (code)`

## Regular Links

A regularlink is just in brackets with the URL in () exactly after it. So here is a link to [SkilStak](https://cnn.com/)
but don;t no line returns inside.

## Automatic Links

These are just detected as is. Aviod the temptation to just type `http...` because not all markdown verisions support autodetection. USe angle brackets!!

<https://skilstak.io>

Certain other links are detected for phone and email:
<rob@skilstak.com>

You can use telephone as well:
<tel:704-661-9972>
## Images 

Images are the same syntax as links but with an ! at the front. The text is the alt text.

![An batman image](Batman.jpg)

Aviod Remote Image Links. Use Local Images.   Also stick with raster images(`.png`, `.jpg`, `.gif`, ) since `.svg`, is not supported everywhere

## Linked Images

Images can be used as Links

[![An batman image](Batman.jpg)](https://www.cnn.com)

## Bulleted Lists

Just use the star `*` version:

* Something
* Here
## Ordered Lists

Begin items in an ordered list with `1.` 
*always*

1. one
1. two
1. three

Avoid nested Lists since they do not copy to other services like Medium.com

## Blocks

Many ways to do blocks but here ae the mains.

Plain as-is block uses more than four spaces.

    Roses are red 
    Violets are blue

You can also use a backtick fence. 
```
    Roses are red 
    Violets are blue
```

##Block Quotes
Begin with an angle bracket `>`

> Here is a quote.

Someone

>:) Hello World, We are feeling good today.

## Wrapping

Generally just write long lines that auto wrap for you so that they look good no matter what.

But this 
also
works.
It is just not consistent. 

## Code Fences

Code fences highlight code blocks for you

```js
console.log("Hello")
```

You can also use tildes `~` so that you can put markdwon itself into code blocks.

~~~

~~~
Reserve tildes mostly for markdwon, use backticks for everything else. 

Markdown Tutorial







